package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.Race;
import org.junit.Assert;
import org.junit.Test;

import static com.leroy.codingame.pods.SampleDiamondRace.*;

public class RaceShould {

    private Race race = RACE;

    @Test
    public void retreive_the_right_checkpoint_position() {
        Assert.assertEquals(CENTER, race.getCheckpointAt(0));
        Assert.assertEquals(EAST, race.getCheckpointAt(1));
        Assert.assertEquals(SOUTH, race.getCheckpointAt(2));
        Assert.assertEquals(WEST, race.getCheckpointAt(3));
        Assert.assertEquals(NORTH, race.getCheckpointAt(4));
        Assert.assertEquals(CENTER, race.getCheckpointAt(5));
        Assert.assertEquals(EAST, race.getCheckpointAt(6));
    }

    @Test
    public void be_writable() {
        Assert.assertEquals(
                "Race{laps=3, checkpoints=[Position{x=0, y=0}, Position{x=10000, y=0}, Position{x=0, y=10000}, Position{x=-10000, y=0}, Position{x=0, y=-10000}]}",
                race.toString()
        );
    }
}
