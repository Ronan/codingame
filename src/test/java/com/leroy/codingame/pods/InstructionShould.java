package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.Pod;
import com.leroy.codingame.pods.Player.Position;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static com.leroy.codingame.pods.Player.*;

public class InstructionShould {

    private Pod centralPod;

    @Before
    public void setUp() {
        centralPod = new Pod(new Position(0, 0), null, -1, -1);
    }

    @Test
    public void produce_an_output() {
        Instruction instruction = new Instruction(0, "0");
        Assert.assertEquals("1000 0 0", instruction.toOutput(centralPod));
    }

    @Test
    public void output_east_full_speed() {
        Instruction instruction = new Instruction(0, "100");
        Assert.assertEquals("1000 0 100", instruction.toOutput(centralPod));
    }

    @Test
    public void output_south_full_speed() {
        Instruction instruction = new Instruction(90, "100");
        Assert.assertEquals("0 1000 100", instruction.toOutput(centralPod));
    }

    @Test
    public void output_north_west_full_speed() {
        Instruction instruction = new Instruction(135, "100");
        Assert.assertEquals("-707 707 100", instruction.toOutput(centralPod));
    }

    @Test
    public void output_north_west_full_speed_when_pod_location_is_5000_2000() {
        Pod pod = new Pod(new Position(5000, 2000), null, -1, -1);

        Instruction instruction = new Instruction(135, "100");
        Assert.assertEquals("4293 2707 100", instruction.toOutput(pod));
    }

    @Test
    public void assume_boost_speed_is_650() {
        Instruction instruction = new Instruction(0, "BOOST");
        Assert.assertEquals(650, instruction.getSpeed());
    }

    @Test
    public void assume_shield_speed_is_0() {
        Instruction instruction = new Instruction(0, "SHIELD");
        Assert.assertEquals(0, instruction.getSpeed());
    }

    @Test
    public void be_equals_to_self() {
        Instruction instruction1 = new Instruction(0, "100");
        Instruction instruction2 = new Instruction(0, "100");

        Assert.assertEquals(instruction1, instruction2);
    }

    @Test
    public void have_an_hashcode() {
        Set<Instruction> set = new HashSet<>();
        set.add(new Instruction(0, "100"));
        set.add(new Instruction(0, "100"));

        Assert.assertEquals(1, set.size());
    }
}
