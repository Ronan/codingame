package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class PilotShould {

    private static final Position POSITION_CENTER = new Position(0, 0);
    private static final Position SPEED_IDLE = new Position(0, 0);
    private static final Position SPEED_EAST = new Position(100, 0);
    private static final Position SPEED_WEST = new Position(-100, 0);

    private Pod POD_IDLE = new Pod(POSITION_CENTER, SPEED_IDLE, 0, -1);
    private Pod POD_MOVING_EAST = new Pod(POSITION_CENTER, SPEED_EAST, 0, -1);
    private Pod POD_MOVING_WEST = new Pod(POSITION_CENTER, SPEED_WEST, 180, -1);
    private Pod POD_HEADING_NORTH = new Pod(POSITION_CENTER, SPEED_EAST, -90, -1);

    private Pilot pilot;

    @Before
    public void setUp() {
        pilot = new Pilot(SampleDiamondRace.RACE) {
            @Override
            Player.PodPrediction getBestChoice(Pod pod) {
                return null;
            }
        };
    }

    @Test
    public void have_idle_option_on_start() {
        List<Instruction> options = pilot.getAllOptions(POD_IDLE);

        Assert.assertTrue(options.contains(new Instruction(0, "0")));
    }


    @Test
    public void have_the_boost_option_on_start() {
        List<Instruction> options = pilot.getAllOptions(POD_IDLE);

        Assert.assertTrue(options.contains(new Instruction(0, "BOOST")));
    }

    @Test
    public void not_have_the_boost_if_already_done() {
        pilot.mayBoost = false;
        List<Instruction> options = pilot.getAllOptions(POD_IDLE);

        Assert.assertFalse(options.contains(new Instruction(0, "BOOST")));
    }

    @Test
    public void have_the_shield() {
        List<Instruction> options = pilot.getAllOptions(POD_IDLE);

        Assert.assertTrue(options.contains(new Instruction(0, "SHIELD")));
    }

    @Test
    public void have_the_option_to_go_full_thrust_on_start() {
        List<Instruction> options = pilot.getAllOptions(POD_IDLE);

        Assert.assertTrue(options.contains(new Instruction(0, "100")));
    }

    @Test
    public void build_all_possible_instruction_at_start() {
        List<Instruction> options = pilot.getAllOptions(POD_IDLE);

        Assert.assertEquals(360*103, options.size());
    }

    @Test
    public void build_all_possible_instruction_when_idle_if_boost_already_done() {
        pilot.mayBoost = false;
        List<Instruction> options = pilot.getAllOptions(POD_IDLE);

        Assert.assertEquals(360*102, options.size());
    }

    @Test
    public void build_all_possible_instruction_when_moving_east() {
        pilot.mayBoost = false;

        List<Instruction> options = pilot.getAllOptions(POD_MOVING_EAST);

        Assert.assertEquals(37*102, options.size());
        Assert.assertEquals(18,
                options.stream()
                        .mapToInt(i -> i.angle.value)
                        .max()
                        .orElse(-1)
        );
        Assert.assertEquals(-18,
                options.stream()
                        .mapToInt(i -> i.angle.value)
                        .min()
                        .orElse(-1)
        );
    }

    @Test
    public void build_all_possible_instruction_when_may_boost() {
        List<Instruction> options = pilot.getAllOptions(POD_MOVING_EAST);

        Assert.assertEquals(37*103, options.size());
    }

    @Test
    public void build_all_possible_instruction_when_moving_north() {
        pilot.mayBoost = false;

        List<Instruction> options = pilot.getAllOptions(POD_HEADING_NORTH);

        Assert.assertEquals(37*102, options.size());
        Assert.assertEquals(-72,
                options.stream()
                        .mapToInt(i -> i.angle.value)
                        .max()
                        .orElse(-1)
        );
        Assert.assertEquals(-108,
                options.stream()
                        .mapToInt(i -> i.angle.value)
                        .min()
                        .orElse(-1)
        );
    }

    @Test
    public void build_all_possible_instruction_when_moving_west() {
        pilot.mayBoost = false;

        List<Instruction> options = pilot.getAllOptions(POD_MOVING_WEST);

        Assert.assertEquals(37*102, options.size());
    }
}
