package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static com.leroy.codingame.pods.SampleDiamondRace.CENTER;
import static com.leroy.codingame.pods.SampleDiamondRace.EAST;
import static com.leroy.codingame.pods.SampleDiamondRace.SOUTH;
import static com.leroy.codingame.pods.SampleDiamondRace.WEST;
import static com.leroy.codingame.pods.SampleDiamondRace.NORTH;

public class PilotWithShieldShould {

    private Race race = new Race(3, Arrays.asList(CENTER, EAST, SOUTH, WEST, NORTH));

    private PilotWithShield shield = new PilotWithShield(race);
    private Pilot brutal = new PilotBrutal(race);

    @Test
    public void active_shield_when_can_glide_to_checkpoint_within_3_ticks() {
        // 3 ticks glide from speed of 100 : 100 + 85 + 72 = 257
        Pod pod = new Pod(new Position(EAST.x - 250 - 600, EAST.y), new Position(100, 0), 0, 1);

        Instruction instruction = shield.chooseNextInstruction(pod);

        Assert.assertEquals("SHIELD", instruction.command);
    }

    @Test
    public void not_active_shield_when_cant_glide_to_checkpoint_within_3_ticks() {
        Pod pod = new Pod(new Position(EAST.x - 260 - 600, EAST.y), new Position(100, 0), 0, 1);

        Instruction shieldInstruction = shield.chooseNextInstruction(pod);
        Instruction brutalInstruction = brutal.chooseNextInstruction(pod);

        Assert.assertEquals(brutalInstruction, shieldInstruction);
    }

    @Test
    public void can_glide_to_next_checkpoint_within_3_ticks() {
        Pod pod = new Pod(new Position(EAST.x - 250 - 600, EAST.y), new Position(100, 0), 0, 1);

        Assert.assertTrue(shield.canGlideToNextCheckpointWithinTicks(3, pod));
    }
}
