package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static com.leroy.codingame.pods.SampleDiamondRace.CENTER;
import static com.leroy.codingame.pods.SampleDiamondRace.EAST;
import static com.leroy.codingame.pods.SampleDiamondRace.SOUTH;
import static com.leroy.codingame.pods.SampleDiamondRace.WEST;
import static com.leroy.codingame.pods.SampleDiamondRace.NORTH;

public class PilotBrutalComparatorShould {

    private static final Instruction IDLE = new Instruction(0, "0");

    private static final Position NOSPEED = new Position(0, 0);

    private Race race = new Race(3, Arrays.asList(CENTER, EAST, SOUTH, WEST, NORTH));

    @Test
    public void prefer_any_instruction_rather_than_no_instruction() {
        PodPrediction prediction = new PodPrediction(
                IDLE,
                new Pod(CENTER, NOSPEED, 0, 1)
        );

        PilotBrutalComparator comparator = new PilotBrutalComparator(race.getCheckpointAt(1));

        Assert.assertEquals(-1, comparator.compare(prediction, null));
    }

    @Test
    public void compare_equals_predictions() {
        PodPrediction prediction = new PodPrediction(
                IDLE,
                new Pod(CENTER, NOSPEED, 0, 1)
        );

        PilotBrutalComparator comparator = new PilotBrutalComparator(race.getCheckpointAt(1));

        Assert.assertEquals(0, comparator.compare(prediction, prediction));

    }

    @Test
    public void prefer_a_closer_position() {
        PodPrediction centerTargetEast = new PodPrediction(IDLE, new Pod(CENTER, NOSPEED, 0, 1));
        PodPrediction westTargetEast = new PodPrediction(IDLE, new Pod(WEST, NOSPEED, 0, 1));

        PilotBrutalComparator comparator = new PilotBrutalComparator(race.getCheckpointAt(1));

        Assert.assertEquals(-1, comparator.compare(centerTargetEast, westTargetEast));
        Assert.assertEquals(1, comparator.compare(westTargetEast, centerTargetEast));
    }

    @Test
    public void prefer_not_turning_when_headed_to_target() {
        PodPrediction centerTurnLeft = new PodPrediction(IDLE, new Pod(CENTER, NOSPEED, -18, 1));
        PodPrediction centerNoTurn = new PodPrediction(IDLE, new Pod(CENTER, NOSPEED, 0, 1));
        PodPrediction centerTurnRight = new PodPrediction(IDLE, new Pod(CENTER, NOSPEED, 18, 1));

        PilotBrutalComparator comparator = new PilotBrutalComparator(race.getCheckpointAt(1));

        Assert.assertEquals(-1, comparator.compare(centerNoTurn, centerTurnLeft));
        Assert.assertEquals(-1, comparator.compare(centerNoTurn, centerTurnRight));
    }

    @Test
    public void prefer_turning_right() {
        PodPrediction centerTurnRight = new PodPrediction(IDLE, new Pod(CENTER, NOSPEED, -178, 1));
        PodPrediction centerHeadWestDeviateNorth = new PodPrediction(IDLE, new Pod(CENTER, NOSPEED, -179, 1));
        PodPrediction centerTurnLeft = new PodPrediction(IDLE, new Pod(CENTER, NOSPEED, -180, 1));

        PilotBrutalComparator comparator = new PilotBrutalComparator(race.getCheckpointAt(1));

        Assert.assertEquals(-1, comparator.compare(centerTurnRight, centerHeadWestDeviateNorth));
        Assert.assertEquals(-1, comparator.compare(centerHeadWestDeviateNorth, centerTurnLeft));
    }

    @Test
    public void prefer_turning_left() {
        PodPrediction centerTurnRight = new PodPrediction(IDLE, new Pod(CENTER, NOSPEED, 180, 1));
        PodPrediction centerHeadWestDeviateNorth = new PodPrediction(IDLE, new Pod(CENTER, NOSPEED, 179, 1));
        PodPrediction centerTurnLeft = new PodPrediction(IDLE, new Pod(CENTER, NOSPEED, 178, 1));

        PilotBrutalComparator comparator = new PilotBrutalComparator(race.getCheckpointAt(1));

        Assert.assertEquals(1, comparator.compare(centerTurnRight, centerHeadWestDeviateNorth));
        Assert.assertEquals(1, comparator.compare(centerHeadWestDeviateNorth, centerTurnLeft));
    }

}
