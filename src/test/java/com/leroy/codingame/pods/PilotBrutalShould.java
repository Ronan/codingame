package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.*;
import org.junit.Assert;
import org.junit.Test;

public class PilotBrutalShould {

    private static final Pod POD_REACHING_EAST
            = new Pod(
            new Position(SampleDiamondRace.EAST.x-700, SampleDiamondRace.EAST.y),
            new Position(100, 0), 0, 1
    );

    private static final Pod POD_REACHING_NORTH
            = new Pod(
            new Position(SampleDiamondRace.NORTH.x, SampleDiamondRace.NORTH.y +700),
            new Position(0, -100), -90, 4
    );

    private Pilot pilot = new PilotBrutal(SampleDiamondRace.RACE);

    @Test
    public void boost_to_target() {
        Pod pod = new Pod(SampleDiamondRace.CENTER, new Position(0, 0), 0, 1);

        Instruction actual = pilot.chooseNextInstruction(pod);

        Instruction expected = new Instruction(0, "BOOST");
        Assert.assertEquals(expected, actual);
    }


    @Test
    public void not_boost_twice() {
        Pilot pilot = new PilotBrutal(SampleDiamondRace.RACE);

        Pod pod = new Pod(SampleDiamondRace.CENTER, new Position(100, 0), 0, 1);
        Instruction first = pilot.chooseNextInstruction(pod);
        Instruction expectedFirst = new Instruction(0, "BOOST");

        Pod pod2 = pilot.physics.move(pod, first);

        Instruction second = pilot.chooseNextInstruction(pod2);
        Instruction expectedSecond = new Instruction(0, "100");

        Assert.assertEquals(expectedFirst, first);
        Assert.assertEquals(expectedSecond, second);
    }

    @Test
    public void be_accurate_when_checkpoint_is_reached() {
        Physics physics = new Physics();
        pilot.mayBoost = false;

        Instruction instruction = pilot.chooseNextInstruction(POD_REACHING_EAST);
        Pod pod = physics.move(POD_REACHING_EAST, instruction);
        pod = pilot.getPodUpdatedWithNewTarget(pod);

        pilot.chooseNextInstruction(pod);

        int accuracy = pilot.getPredictionsAccuracy();

        Assert.assertEquals(2, pod.target);
        Assert.assertEquals(100, accuracy);
    }

    @Test
    public void be_accurate_when_last_checkpoint_is_reached() {
        Physics physics = new Physics();
        pilot.mayBoost = false;

        Instruction instruction = pilot.chooseNextInstruction(POD_REACHING_NORTH);
        Pod pod = physics.move(POD_REACHING_NORTH, instruction);
        pod = pilot.getPodUpdatedWithNewTarget(pod);

        pilot.chooseNextInstruction(pod);

        int accuracy = pilot.getPredictionsAccuracy();

        Assert.assertEquals(0, pod.target);
        Assert.assertEquals(100, accuracy);
    }


}
