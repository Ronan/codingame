package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.Pilot;
import org.junit.Assert;
import org.junit.Test;

import static com.leroy.codingame.pods.Player.*;

public class PilotAnticipatingShould {

    @Test
    public void start_targeting_next_checkpoint() {
        Pilot pilot = new PilotAnticipating(SampleDiamondRace.RACE);
        Pod pod = new Pod(new Position(SampleDiamondRace.EAST.x - 700, SampleDiamondRace.EAST.y), new Position(100, 0), 0, 1);

        Instruction actual = pilot.chooseNextInstruction(pod);

        Instruction expected = new Instruction(18, "0");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void not_boost_twice() {
        Pilot pilot = new PilotAnticipating(SampleDiamondRace.RACE);

        Pod pod = new Pod(SampleDiamondRace.CENTER, new Position(100, 0), 0, 1);
        Instruction first = pilot.chooseNextInstruction(pod);
        Instruction expectedFirst = new Instruction(0, "BOOST");

        Pod pod2 = pilot.physics.move(pod, first);

        Instruction second = pilot.chooseNextInstruction(pod2);
        Instruction expectedSecond = new Instruction(0, "100");

        Assert.assertEquals(expectedFirst, first);
        Assert.assertEquals(expectedSecond, second);
    }
}