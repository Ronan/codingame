package com.leroy.codingame.pods;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PositionCalculatorShould {

    private PlayerSilver.PositionCalculator calculator;

    @Before
    public void setUp() {
        calculator = new PlayerSilver.PositionCalculator();
    }

    @Test
    public void compute_next_frame_when_idle() {
        PlayerSilver.RaceFrame current = buildRaceFrame(0, 0, 0);
        PlayerSilver.Position speed = new PlayerSilver.Position(0, 0);

        PlayerSilver.RaceFrame expected = buildRaceFrame(1, 0, 0);

        PlayerSilver.RaceFrame next = current.copy();
        next.idx = next.idx + 1;

        next.me = calculator.compute(next.me, speed, 0, 0);

        Assert.assertEquals(
                expected.toString(),
                next.toString()
        );
    }

    @Test
    public void compute_next_frame_when_heading_east() {
        PlayerSilver.RaceFrame current = buildRaceFrame(0, 0, 0);
        PlayerSilver.Position speed = new PlayerSilver.Position(0, 0);

        PlayerSilver.RaceFrame expected = new PlayerSilver.RaceFrame(
                1,
                new PlayerSilver.Position(100, 0),
                0,
                new PlayerSilver.Position(0, 0),
                new PlayerSilver.Position(5000, 5000)
        );

        PlayerSilver.RaceFrame next = current.copy();
        next.idx = next.idx + 1;

        next.me = calculator.compute(next.me, speed, 100, 0);

        Assert.assertEquals(
                expected.toString(),
                next.toString()
        );
    }

    @Test
    public void compute_next_frame_when_heading_south_east() {
        PlayerSilver.RaceFrame current = buildRaceFrame(0, 0, 0);
        PlayerSilver.Position speed = new PlayerSilver.Position(0, 0);

        PlayerSilver.RaceFrame expected = new PlayerSilver.RaceFrame(
                1,
                new PlayerSilver.Position(87, 50),
                0,
                new PlayerSilver.Position(0, 0),
                new PlayerSilver.Position(5000, 5000)
        );

        PlayerSilver.RaceFrame next = current.copy();
        next.idx = next.idx + 1;

        next.me = calculator.compute(next.me, speed, 100, 30);

        Assert.assertEquals(
                expected.toString(),
                next.toString()
        );
    }

    @Test
    public void compute_next_frame_when_heading_north_east() {
        PlayerSilver.RaceFrame current = buildRaceFrame(0, 0, 0);
        PlayerSilver.Position speed = new PlayerSilver.Position(0, 0);

        PlayerSilver.RaceFrame expected = buildRaceFrame(1, 87, -50);

        PlayerSilver.RaceFrame next = current.copy();
        next.idx = next.idx + 1;

        next.me = calculator.compute(next.me, speed, 100, -30);

        Assert.assertEquals(
                expected.toString(),
                next.toString()
        );
    }

    @Test
    public void compute_next_frame_when_heading_north_west() {
        PlayerSilver.RaceFrame current = buildRaceFrame(0, 0, 0);
        PlayerSilver.Position speed = new PlayerSilver.Position(0, 0);

        PlayerSilver.RaceFrame expected = buildRaceFrame(1, -50, -87);

        PlayerSilver.RaceFrame next = current.copy();
        next.idx = next.idx + 1;

        next.me = calculator.compute(next.me, speed, 100, -120);

        Assert.assertEquals(
                expected.toString(),
                next.toString()
        );
    }

    @Test
    public void compute_next_frame_having_previous_speed_when_heading_north_west() {
        PlayerSilver.RaceFrame current = buildRaceFrame(0, 0, 0);
        PlayerSilver.Position speed = new PlayerSilver.Position(100, 100);

        PlayerSilver.RaceFrame expected = buildRaceFrame(1, 35, -2);

        PlayerSilver.RaceFrame next = current.copy();
        next.idx = next.idx + 1;

        next.me = calculator.compute(next.me, speed, 100, -120);

        Assert.assertEquals(
                expected.toString(),
                next.toString()
        );
    }

    private PlayerSilver.RaceFrame buildRaceFrame(int idx, int x, int y) {
        return new PlayerSilver.RaceFrame(
                idx,
                new PlayerSilver.Position(x, y),
                0,
                new PlayerSilver.Position(0, 0),
                new PlayerSilver.Position(5000, 5000)
        );
    }
}
