package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.*;
import org.junit.Assert;
import org.junit.Test;

public class PlayerRunnerShould {

    @Test
    public void declare_player1_as_the_winner() {
        PlayerRunner runner = new PlayerRunner(SampleDiamondRace.RACE);
        Player player = new Player()
                .readFrom(runner)
                .writeTo(runner)
                .logTo(runner)
                .readRace()
                .firstPilotIsA(PilotAnticipating::new)
                .secondPilotIsA(PilotBrutal::new);
        runner.setPilots(player.getPilots());

        Assert.assertEquals(SampleDiamondRace.RACE, player.race);
        Assert.assertEquals("pilot1", player.play());
        Assert.assertEquals(399, player.pilot1.total);
        Assert.assertEquals(100, player.pilot1.getPredictionsAccuracy());
        Assert.assertEquals(399, player.pilot2.total);
        Assert.assertEquals(100, player.pilot2.getPredictionsAccuracy());
    }

    @Test
    public void finish_the_diamond_race_in_421_turn_when_anticipating() {
        PlayerRunner runner = new PlayerRunner(SampleDiamondRace.RACE);
        Player player = new Player()
                .readFrom(runner)
                .writeTo(runner)
                .logTo(runner)
                .readRace()
                .firstPilotIsA(PilotAnticipating::new)
                .secondPilotIsA(PilotIdle::new);
        runner.setPilots(player.getPilots());

        Assert.assertEquals(SampleDiamondRace.RACE, player.race);
        Assert.assertEquals("pilot1", player.play());
        Assert.assertEquals(399, player.pilot2.total);
    }

    @Test
    public void finish_the_diamond_race_in_421_turn_when_brutal() {
        PlayerRunner runner = new PlayerRunner(SampleDiamondRace.RACE);
        Player player = new Player()
                .readFrom(runner)
                .writeTo(runner)
                .logTo(runner)
                .readRace()
                .firstPilotIsA(PilotIdle::new)
                .secondPilotIsA(PilotBrutal::new);
        runner.setPilots(player.getPilots());

        Assert.assertEquals(SampleDiamondRace.RACE, player.race);
        Assert.assertEquals("pilot2", player.play());
        Assert.assertEquals(421, player.pilot2.total);
    }
}
