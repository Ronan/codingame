package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PilotRandomShould {

    private Pilot pilot;

    private static final Pod POD_ON_START
            = new Pod(SampleDiamondRace.CENTER, new Position(0, 0), 0, 1);

    @Before
    public void setUp() {
        pilot = new PilotRandom(SampleDiamondRace.RACE);
    }

    @Test
    public void give_instruction() {
        Pod pod = new Pod(SampleDiamondRace.CENTER, new Position(0, 0), 0, 1);

        Instruction actual = pilot.chooseNextInstruction(pod);

        Assert.assertNotNull(actual);
    }

    @Test
    public void be_accurate_on_start() {
        int accuracy = pilot.getPredictionsAccuracy();
        Assert.assertEquals(100, accuracy);
    }

    @Test
    public void be_inaccurate_when_it_is_not_happenning() {
        pilot.chooseNextInstruction(POD_ON_START);
        pilot.chooseNextInstruction(POD_ON_START);

        int accuracy = pilot.getPredictionsAccuracy();
        Assert.assertEquals(0, accuracy);
    }

    @Test
    public void be_accurate_when_it_is_happenning() {
        Physics physics = new Physics();

        Instruction instruction = pilot.chooseNextInstruction(POD_ON_START);
        Pod pod = physics.move(POD_ON_START, instruction);
        pod = pilot.getPodUpdatedWithNewTarget(pod);

        pilot.chooseNextInstruction(pod);

        int accuracy = pilot.getPredictionsAccuracy();
        Assert.assertEquals(100, accuracy);
    }
}
