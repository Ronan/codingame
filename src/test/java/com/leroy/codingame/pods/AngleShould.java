package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.Angle;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class AngleShould {

    @Test
    public void have_a_value_of_90_for_south() {
        Assert.assertEquals(90, Angle.of(90).value);
    }

    @Test
    public void have_a_value_of_minus_90_for_north() {
        Assert.assertEquals(-90, Angle.of(270).value);
    }

    @Test
    public void have_a_value_of_180_for_west() {
        Assert.assertEquals(180, Angle.of(180).value);
        Assert.assertEquals(180, Angle.of(-180).value);
    }

    @Test
    public void return_correct_cosinus() {
        Assert.assertEquals(-0.5, Angle.of(120).cos(), 0.01);
    }

    @Test
    public void return_correct_sinus() {
        Assert.assertEquals(0.5, Angle.of(30).sin(), 0.01);
    }

    @Test
    public void be_writable() {
        Assert.assertEquals("Angle{value=0}", Angle.of(0).toString());
    }

    @Test
    public void have_an_hashcode() {
        Set<Angle> set = new HashSet<>();
        set.add(Angle.of(0));
        set.add(Angle.of(0));

        Assert.assertEquals(1, set.size());
    }
}
