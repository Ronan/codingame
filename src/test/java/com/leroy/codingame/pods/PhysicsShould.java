package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.Instruction;
import com.leroy.codingame.pods.Player.Physics;
import com.leroy.codingame.pods.Player.Pod;
import com.leroy.codingame.pods.Player.Position;
import org.junit.Assert;
import org.junit.Test;

public class PhysicsShould {

    private Physics physics = new Physics();

    @Test
    public void predict_execution_result() {
        Pod pod = new Pod(new Position(0, 0), new Position(0, 0), 0, 0);
        Pod actual = physics.move(pod, new Instruction(0, "0"));

        Pod expected = new Pod(new Position(0, 0), new Position(0, 0), 0, 0);

        Assert.assertEquals(expected, actual);
    }
    @Test
    public void predict_right_speed_1() {
        Pod pod = new Pod(new Position(4451, 6718), new Position(500, -233), -25, 1);
        Pod actual = physics.move(pod, new Instruction(-25, "100"));

        Pod expected = new Pod(new Position(5042, 6443), new Position(502, -233), -25, 1);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void predict_right_speed_2() {
        Pod pod = new Pod(new Position(11033, 3612), new Position(-341, -92), -171, 1);
        Pod actual = physics.move(pod, new Instruction(-171, "100"));

        Pod expected = new Pod(new Position(10593, 3504), new Position(-373, -91), -171, 1);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void predict_right_speed_3() {
        Pod pod = new Pod(new Position(5473, 2254), new Position(-209, -304), -164, 2);
        Pod actual = physics.move(pod, new Instruction(178, "100"));

        Pod expected = new Pod(new Position(5164, 1953), new Position(-262, -255), 178, 2);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void predict_right_speed_4() {
        Pod pod = new Pod(new Position(9627, 5802), new Position(186, 0), -37, 2);
        Pod actual = physics.move(pod, new Instruction(-45, "100"));

        Pod expected = new Pod(new Position(9884, 5731), new Position(218, -60), -45, 2);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void adjust_speed() {
        Assert.assertEquals(502, physics.adjustSpeed(590.630778703665));
        Assert.assertEquals(-373, physics.adjustSpeed(-439.7688340595138));
        Assert.assertEquals(-262, physics.adjustSpeed(-308.93908270190957));
        Assert.assertEquals(-60, physics.adjustSpeed(-70.71067811865474));
    }
}
