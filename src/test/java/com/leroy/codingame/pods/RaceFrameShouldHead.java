package com.leroy.codingame.pods;

import com.leroy.codingame.pods.PlayerSilver.RaceFrame;
import org.junit.Assert;
import org.junit.Test;

import static com.leroy.codingame.pods.PlayerSilver.*;

public class RaceFrameShouldHead {

    @Test
    public void east_when_correction_is_45_right_and_target_is_south_east() {
        RaceFrame frame = new RaceFrame(0,
                new Position(0, 0),
                45,
                null,
                new Position(5, 5)
        );
        Assert.assertEquals(0, frame.getHeading(), 0.1d);
    }

    @Test
    public void east_when_correction_is_45_left_and_target_is_north_east() {
        RaceFrame frame = new RaceFrame(0,
                new Position(0, 0),
                -45,
                null,
                new Position(5, -5)
        );
        Assert.assertEquals(0, frame.getHeading(), 0.1d);
    }

    @Test
    public void south_when_correction_is_45_left_and_target_is_south_east() {
        RaceFrame frame = new RaceFrame(0,
                new Position(0, 0),
                -45,
                null,
                new Position(5, 5)
        );
        Assert.assertEquals(90, frame.getHeading(), 0.1d);
    }

    @Test
    public void north_when_correction_is_135_right_and_target_is_south_east() {
        RaceFrame frame = new RaceFrame(0,
                new Position(0, 0),
                135,
                null,
                new Position(5, 5)
        );
        Assert.assertEquals(-90, frame.getHeading(), 0.1d);
    }

    @Test
    public void east_when_correction_is_135_right_and_target_is_south_west() {
        RaceFrame frame = new RaceFrame(0,
                new Position(0, 0),
                135,
                null,
                new Position(-5, 5)
        );
        Assert.assertEquals(0, frame.getHeading(), 0.1d);
    }
}
