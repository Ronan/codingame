package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.Position;
import com.leroy.codingame.pods.Player.Race;

import java.util.Arrays;

class SampleDiamondRace {

    static final Position CENTER = new Position(0, 0);
    static final Position EAST = new Position(10000, 0);
    static final Position SOUTH = new Position(0, 10000);
    static final Position WEST = new Position(-10000, 0);
    static final Position NORTH = new Position(0, -10000);

    static final Race RACE = new Race(3, Arrays.asList(CENTER, EAST, SOUTH, WEST, NORTH));

}
