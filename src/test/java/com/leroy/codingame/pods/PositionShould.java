package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.Angle;
import com.leroy.codingame.pods.Player.Position;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class PositionShould {

    private Position center = new Position(0 , 0);
    private Position north = new Position(0 , -10);
    private Position south = new Position(0 , 10);
    private Position northwest = new Position(-10 , -10);

    @Test
    public void have_a_distance_to_itself_of_zero() {
        Assert.assertEquals(0, center.distanceTo(center), 0.01);
    }

    @Test
    public void have_a_distance_to_north_of_10() {
        Assert.assertEquals(10.0, center.distanceTo(north), 0.01);
    }

    @Test
    public void have_a_distance_to_northwest_of_10() {
        Assert.assertEquals(14.14, center.distanceTo(northwest), 0.01);
    }

    @Test
    public void have_an_angle_to_itself_of_0() {
        Assert.assertEquals(Angle.of(0), center.angleTo(center));
    }

    @Test
    public void have_an_angle_to_north_of_minus_90() {
        Assert.assertEquals(Angle.of(-90), center.angleTo(north));
    }

    @Test
    public void have_an_angle_to_south_of_90() {
        Assert.assertEquals(Angle.of(90), center.angleTo(south));
    }

    @Test
    public void have_an_angle_to_north_west_of_minus_135() {
        Assert.assertEquals(Angle.of(-135), center.angleTo(northwest));
    }

    @Test
    public void be_writable() {
        Assert.assertEquals("Position{x=0, y=0}", center.toString());
    }

    @Test
    public void be_equals_to_self() {
        Assert.assertEquals(new Position(0 , 0), center);
    }

    @Test
    public void have_an_hashcode() {
        Set<Position> set = new HashSet<>();
        set.add(new Position(0, 0));
        set.add(center);

        Assert.assertEquals(1, set.size());
    }
}
