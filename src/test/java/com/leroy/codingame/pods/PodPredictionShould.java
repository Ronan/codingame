package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static com.leroy.codingame.pods.SampleDiamondRace.CENTER;
import static com.leroy.codingame.pods.SampleDiamondRace.EAST;
import static com.leroy.codingame.pods.SampleDiamondRace.SOUTH;
import static com.leroy.codingame.pods.SampleDiamondRace.WEST;
import static com.leroy.codingame.pods.SampleDiamondRace.NORTH;

public class PodPredictionShould {

    private static final Instruction IDLE = new Instruction(0, "0");

    private static final Position NOSPEED = new Position(0, 0);

    private Race race = new Race(3, Arrays.asList(CENTER, EAST, SOUTH, WEST, NORTH));

    @Test
    public void be_writable() {
        PodPrediction centerTargetEast = new PodPrediction(IDLE, new Pod(CENTER, NOSPEED, 0, 1));
        Assert.assertEquals(
                "PodPrediction{instruction=Instruction{angle=Angle{value=0}, command='0'}, pod=Pod{position=Position{x=0, y=0}, speed=Position{x=0, y=0}, heading=Angle{value=0}, target=1}}",
                centerTargetEast.toString()
        );
    }

    @Test
    public void be_equals_to_self() {
        PodPrediction prediction1 = new PodPrediction(
                IDLE,
                new Pod(new Position(13090, 7674), new Position(0, 0), 1, 1)
        );
        PodPrediction prediction2 = new PodPrediction(
                IDLE,
                new Pod(new Position(13090, 7674), new Position(0, 0), 1, 1)
        );
        Assert.assertEquals(prediction1, prediction2);
    }

    @Test
    public void have_an_hashcode() {
        PodPrediction prediction1 = new PodPrediction(
                IDLE,
                new Pod(new Position(13090, 7674), new Position(0, 0), 1, 1)
        );
        PodPrediction prediction2 = new PodPrediction(
                IDLE,
                new Pod(new Position(13090, 7674), new Position(0, 0), 1, 1)
        );

        Set<PodPrediction> set = new HashSet<>();
        set.add(prediction1);
        set.add(prediction2);

        Assert.assertEquals(1, set.size());
    }
}
