package com.leroy.codingame.pods;

import com.leroy.codingame.pods.Player.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class PlayerRunner implements PlayerInput, PlayerOutput, PlayerLogger {

    private List<Integer> output;
    private List<Pilot> pilots;

    PlayerRunner(Race race) {
        this.output = new ArrayList<>(getRaceData(race));
        for (int i = 0; i < 4; i++) {
            this.output.addAll(Arrays.asList(0, 0, 0, 0, 0, 1));
        }
    }

    void setPilots(List<Pilot> pilots) {
        this.pilots = pilots;
    }

    @Override
    public int read() {
        if (output.size() == 0) {
            throw new RuntimeException("Nothing to read !");
        }
        return output.remove(0);
    }

    @Override
    public void write(String message) {
        if (this.pilots.get(0).getInstruction() == null) {
            this.pilots.get(0).setInstruction(message);
        } else {
            this.pilots.get(1).setInstruction(message);
        }

        if (this.pilots.get(1).getInstruction() != null) {
            playTurn();
        }
    }

    private void playTurn() {
        this.pilots.stream()
                .map(Pilot::playTurn)
                .forEach(output::addAll);
    }

    @Override
    public void debug(String message) {
    }

    private List<Integer> getRaceData(Race race) {
        List<Integer> result = new ArrayList<>(Arrays.asList(race.laps, race.checkpoints.size()));
        for (int i = 0; i < race.checkpoints.size(); i++) {
            result.addAll(Arrays.asList(race.getCheckpointAt(i).x, race.getCheckpointAt(i).y));
        }
        return result;
    }

}
