package com.leroy.codingame.pods;

import java.util.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class PlayerSilver {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        List<RaceFrame> frames = new ArrayList<>();
        // game loop
        while (true) {
            int x = in.nextInt();
            int y = in.nextInt();
            int nextCheckpointX = in.nextInt(); // x position of the next check point
            int nextCheckpointY = in.nextInt(); // y position of the next check point
            int nextCheckpointDist = in.nextInt(); // distance to the next checkpoint
            int nextCheckpointAngle = in.nextInt(); // angle between your pod orientation and the direction of the next checkpoint
            int opponentX = in.nextInt();
            int opponentY = in.nextInt();

            Position me = new Position(x, y);
            Position him = new Position(opponentX, opponentY);
            Position target = new Position(nextCheckpointX, nextCheckpointY);
            RaceFrame current = new RaceFrame(frames.size()+1, me, nextCheckpointAngle, him, target);
            frames.add(0, current);

            String instructions = getInstructions(frames);

            // You have to output the target position
            // followed by the power (0 <= thrust <= 100)
            // i.e.: "x y thrust"
            System.out.println(instructions);
        }
    }

    protected static String getInstructions(List<RaceFrame> frames) {
        Position speed = new Position(0, 0);
        if (frames.size() > 1) {
            speed = getSpeed(frames.get(0).me, frames.get(1).me);
        }

        RaceFrame current = frames.get(0);
        System.err.println("current ...........: " + current);
        System.err.println("speed .............: " + speed);
        System.err.println("distance to target : " + current.me.distanceTo(current.target));
        System.err.println("distance to him .. : " + current.me.distanceTo(current.him));

        Position direction = getBestDirection(current, speed);
        String command = "BOOST";
        if (current.me.distanceTo(current.target) < 900) {
            command = "40";
        }

        return direction.x + " " + direction.y + " "+command;
    }

    private static Position getBestDirection(RaceFrame current, Position speed) {
        PositionCalculator calculator = new PositionCalculator();
        Position bestNext = null;
        int bestAngle = 360;
        for (int angle = -180; angle < 180; angle++) {
            Position candidate = calculator.compute(current.me, speed, 100, angle);
            double dist = candidate.distanceTo(current.target);
            if (bestNext == null || dist < bestNext.distanceTo(current.target)) {
                bestNext = candidate;
                bestAngle = angle;
            }
        }
        return new Position(
                current.me.x + (int)Math.round(500*Math.cos(Math.toRadians(bestAngle))),
                current.me.y + (int)Math.round(500*Math.sin(Math.toRadians(bestAngle)))
        );
    }

    private static Position getSpeed(Position current, Position previous) {
        Position speed;
        if (previous != null) {
            speed = new Position(current.x - previous.x, current.y - previous.y);
        } else {
            speed = new Position(0, 0);
        }
        return speed;
    }

    public static class PositionCalculator {

        public Position compute(Position initial, Position speed, int thrust, double heading) {
            Position nextPos = initial.copy();

            Position speed2 = speed.copy();
            speed2.x = speed2.x * 85 / 100
                    + (int)Math.round(thrust * Math.cos(Math.toRadians(heading)));
            speed2.y = speed2.y * 85 / 100
                    + (int)Math.round(thrust * Math.sin(Math.toRadians(heading)));

            nextPos.x = nextPos.x + speed2.x;
            nextPos.y = nextPos.y + speed2.y;
            return nextPos;
        }

    }

    public static class RaceFrame {

        int idx;
        Position me;
        int targetAngle;
        Position him;
        Position target;

        RaceFrame(int idx, Position me, int targetAngle, Position him, Position target) {
            this.idx = idx;
            this.me = me;
            this.targetAngle = targetAngle;
            this.him = him;
            this.target = target;
        }

        public double getHeading() {
            double heading = me.angleTo(target) - targetAngle;
            if (heading > 180)
                heading = heading -360;
            if (heading < -180)
                heading = heading +360;
            return heading;
        }

        public RaceFrame copy() {
            return new RaceFrame(idx, me.copy(), targetAngle, him.copy(), target.copy());
        }

        public String toString() {
            return idx+","+me.x+","+me.y+","+him.x+","+him.y+","+target.x+","+target.y;
        }
    }

    public static class Position {
        int x;
        int y;

        public Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public double distanceTo(Position that) {
            return Math.sqrt(
                    Math.pow(this.x-that.x, 2) +
                            Math.pow(this.y-that.y, 2)
            );
        }

        public double angleTo(Position that) {
            double alpha = Math.toDegrees(Math.asin((that.y-this.y)/this.distanceTo(that)));
            if (this.x > that.x) {
                alpha = 180 - alpha;
            }
            return alpha;
        }

        public Position copy() {
            return new Position(this.x, this.y);
        }

        public String toString() {
            return "{x:"+x+",y:"+y+"}";
        }
    }
}