package com.leroy.codingame.pods;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

    // 320
    public static void main(String args[]) {
        new Player()
                .readFrom(new Scanner(System.in)::nextInt)
                .writeTo(System.out::println)
                .logTo(System.err::println)
                .readRace()
                .firstPilotIsA(PilotAnticipating::new)
                .secondPilotIsA(PilotAnticipating::new)
                .play();
    }

    private PlayerInput input;
    private PlayerOutput output;
    private PlayerLogger log;

    Race race;
    Pilot pilot1;
    Pilot pilot2;
    private Pilot pilot3;
    private Pilot pilot4;

    Player readFrom(PlayerInput input) {
        this.input = input;
        return this;
    }

    Player writeTo(PlayerOutput output) {
        this.output = output;
        return this;
    }

    Player logTo(PlayerLogger log) {
        this.log = log;
        return this;
    }

    Player readRace() {
        race = new RaceBuilder().from(input).build();
        pilot3 = new PilotIdle(race);
        this.pilot3.name = "pilot3";
        pilot4 = new PilotIdle(race);
        this.pilot4.name = "pilot4";
        return this;
    }

    Player firstPilotIsA(Function<Race, Pilot> pilot) {
        this.pilot1 = pilot.apply(this.race);
        this.pilot1.name = "pilot1";
        return this;
    }

    Player secondPilotIsA(Function<Race, Pilot> pilot) {
        this.pilot2 = pilot.apply(this.race);
        this.pilot2.name = "pilot2";
        return this;
    }

    String play() {
        // game loop
        String winner;
        try {
            while (true) {
                Pod myFirstPod = new PodBuilder().from(input).build();
                pilot1.setPod(myFirstPod);
                pilot1.target = myFirstPod.target;
                Pod mySecondPod = new PodBuilder().from(input).build();
                pilot2.setPod(mySecondPod);
                pilot2.target = mySecondPod.target;
                Pod hisFirstPod = new PodBuilder().from(input).build();
                pilot3.setPod(hisFirstPod);
                pilot3.target = hisFirstPod.target;
                Pod hisSecondPod = new PodBuilder().from(input).build();
                pilot4.setPod(hisSecondPod);
                pilot4.target = hisSecondPod.target;

                // Write an action using System.out.println()
                // To debug: System.err.println("Debug messages...");
                log.debug("race ........: " + race);

                Position firstTarget = race.getCheckpointAt(myFirstPod.target);
                log.debug("myFirstPod ..: " + myFirstPod + " / " + myFirstPod.position.angleTo(firstTarget));

                Position secondTarget = race.getCheckpointAt(mySecondPod.target);
                log.debug("mySecondPod .: " + mySecondPod + " / " + mySecondPod.position.angleTo(secondTarget));

                log.debug("hisFirstPod .: " + hisFirstPod);
                log.debug("hisSecondPod : " + hisSecondPod);

                Instruction first = pilot1.chooseNextInstruction(myFirstPod);
                log.debug("myFirstAccuracy ....: " + pilot1.getPredictionsAccuracy());

                Instruction second = pilot2.chooseNextInstruction(mySecondPod);
                log.debug("mySecondAccuracy ...: " + pilot2.getPredictionsAccuracy());


                // You have to output the target position
                // followed by the power (0 <= thrust <= 100)
                // i.e.: "x y thrust"
                output.write(first.toOutput(myFirstPod));
                output.write(second.toOutput(mySecondPod));
            }
        }catch (RuntimeException finish) {
            winner = finish.getMessage();
        }
        return winner;
    }

    List<Pilot> getPilots() {
        return Arrays.asList(
                pilot1,
                pilot2,
                pilot3,
                pilot4
        );
    }

    interface PlayerInput {
        int read();
    }

    interface PlayerOutput {
        void write(String message);
    }

    interface PlayerLogger {
        void debug(String message);
    }

    static class Angle {

        static Angle of(int value) {
            return new Angle(value);
        }

        final int value;

        private Angle(int value) {
            int normalizedValue = value;
            while (normalizedValue > 180) {
                normalizedValue = normalizedValue -360;
            }
            while (normalizedValue <= -180) {
                normalizedValue = normalizedValue +360;
            }
            this.value = normalizedValue;
        }

        double cos() {
            return Math.cos(Math.toRadians(this.value));
        }

        double sin() {
            return Math.sin(Math.toRadians(this.value));
        }

        @Override
        public String toString() {
            return "Angle{value=" + value + "}";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Angle angle = (Angle) o;
            return value == angle.value;
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }
    }

    static class Position {
        final int x;
        final int y;

        Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        double distanceTo(Position that) {
            return Math.sqrt(
                Math.pow(this.x-that.x, 2) + Math.pow(this.y-that.y, 2)
            );
        }

        Angle angleTo(Position that) {
            double alpha = Math.toDegrees(Math.asin((that.y-this.y)/this.distanceTo(that)));
            if (this.x > that.x) {
                alpha = 180 - alpha;
            }
            return Angle.of((int)Math.round(alpha));
        }

        @Override
        public String toString() {
            return "Position{x=" + x + ", y=" + y + "}";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Position position = (Position) o;
            return x == position.x && y == position.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }

    static class Race {
        final int laps;
        final List<Position> checkpoints;

        Race(int laps, List<Position> checkpoints) {
            this.laps = laps;
            this.checkpoints = Collections.unmodifiableList(checkpoints);
        }

        Position getCheckpointAt(int target) {
            while (target >= checkpoints.size()) {
                target = target - checkpoints.size();
            }
            return this.checkpoints.get(target);
        }

        @Override
        public String toString() {
            return "Race{" +
                    "laps=" + laps +
                    ", checkpoints=" + checkpoints +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Race race = (Race) o;
            return laps == race.laps &&
                    Objects.equals(checkpoints, race.checkpoints);
        }

        @Override
        public int hashCode() {
            return Objects.hash(laps, checkpoints);
        }
    }

    static class Instruction {
        final Angle angle;
        final String command;

        Instruction(int angle, String command) {
            this.angle = Angle.of(angle);
            this.command = command;
        }

        String toOutput(Pod pod) {
            long directionX = pod.position.x + Math.round(1000 * Math.cos(Math.toRadians(this.angle.value)));
            long directionY = pod.position.y + Math.round(1000 * Math.sin(Math.toRadians(this.angle.value)));

            return directionX+" "+directionY+" "+this.command;
        }

        int getSpeed() {
            int speed;
            if ("BOOST".equals(command)) {
                speed = 650;
            } else if ("SHIELD".equals(command)) {
                speed = 0;
            } else {
                speed = Integer.valueOf(command);
            }
            return speed;
        }

        @Override
        public String toString() {
            return "Instruction{angle=" + angle + ", command='" + command + "\'}";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Instruction that = (Instruction) o;
            return Objects.equals(angle, that.angle) &&
                    Objects.equals(command, that.command);
        }

        @Override
        public int hashCode() {
            return Objects.hash(angle, command);
        }
    }

    static class Pod {
        final Position position;
        final Position speed;
        final Angle heading;
        final int target;

        Pod(Position position, Position speed, int heading, int target) {
            this.position = position;
            this.speed = speed;
            this.heading = Angle.of(heading);
            this.target = target;
        }

        @Override
        public String toString() {
            return "Pod{" +
                    "position=" + position +
                    ", speed=" + speed +
                    ", heading=" + heading +
                    ", target=" + target +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pod pod = (Pod) o;
            return target == pod.target &&
                    Objects.equals(position, pod.position) &&
                    Objects.equals(speed, pod.speed) &&
                    Objects.equals(heading, pod.heading);
        }

        @Override
        public int hashCode() {
            return Objects.hash(position, speed, heading, target);
        }
    }

    static class Physics {
        Pod move(Pod pod, Instruction instruction) {
            double speedX = pod.speed.x + instruction.angle.cos() * instruction.getSpeed();
            double speedY = pod.speed.y + instruction.angle.sin() * instruction.getSpeed();

            double posX = pod.position.x + speedX;
            double posY = pod.position.y + speedY;
            Position nextPos = new Position((int)Math.round(posX), (int)Math.round(posY));

            return new Pod(
                    nextPos,
                    new Position(adjustSpeed(speedX), adjustSpeed(speedY)),
                    instruction.angle.value,
                    pod.target
            );
        }

        int adjustSpeed(double initial) {
            return (int)(initial * 0.85);
        }
    }

    static class PodPrediction {

        final Instruction instruction;
        final Pod pod;

        PodPrediction(Instruction instruction, Pod pod) {
            this.instruction = instruction;
            this.pod = pod;
        }

        @Override
        public String toString() {
            return "PodPrediction{" +
                    "instruction=" + instruction +
                    ", pod=" + pod +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PodPrediction that = (PodPrediction) o;
            return Objects.equals(instruction, that.instruction) &&
                    Objects.equals(pod, that.pod);
        }

        @Override
        public int hashCode() {
            return Objects.hash(instruction, pod);
        }
    }

    static abstract class Pilot {
        final Race race;
        final Physics physics = new Physics();

        boolean mayBoost = true;

        private PodPrediction lastPrediction = null;
        int total = 0;
        int accurate = 0;

        String name;
        private Pod pod;
        private int target;
        private int laps;
        private String instruction;

        Pilot(Race race) {
            this.race = race;
        }

        Instruction chooseNextInstruction(Pod pod) {
            if (this.lastPrediction != null) {
                this.total++;
                if (this.lastPrediction.pod.equals(pod)) {
                    this.accurate++;
                }
            }
            PodPrediction prediction = getBestChoice(pod);
            if ("BOOST".equals(prediction.instruction.command)) {
                this.mayBoost = false;
            }

            prediction = new PodPrediction(prediction.instruction, getPodUpdatedWithNewTarget(prediction.pod));

            this.lastPrediction = prediction;
            return prediction.instruction;
        }

        List<Integer> playTurn() {
            Pod pod = this.getPod();
            Instruction instruction = fromOutput(this.getInstruction(), pod.position);
            pod = physics.move(pod, instruction);

            pod = getPodUpdatedWithNewTarget(pod);
            if (pod.target == 0 && this.getPod().target != 0) {
                this.setLaps(this.getLaps()+1);
            }
            if (this.getLaps() == race.laps && pod.target == 1) {
                throw new RuntimeException(this.getName());
            }
            this.setInstruction(null);
            this.setPod(pod);
            return getPodData();
        }

        Pod getPodUpdatedWithNewTarget(Pod pod) {
            if (race.getCheckpointAt(pod.target).distanceTo(pod.position) < 600) {
                int nextTarget = pod.target + 1;
                if (nextTarget >= race.checkpoints.size()) {
                    nextTarget = 0;
                }
                pod = new Pod(
                        pod.position,
                        pod.speed,
                        pod.heading.value,
                        nextTarget
                );
            }
            return pod;
        }

        abstract PodPrediction getBestChoice(Pod pod);

        List<Instruction> getAllOptions(Pod pod) {
            return getAllOptions(pod, true);
        }

        List<Instruction> getAllOptions(Pod pod, boolean mayShield) {
            int minAngle = 1;
            int maxAngle = 360;
            if (pod.speed.x != 0 || pod.speed.y != 0) {
                minAngle = pod.heading.value - 18;
                maxAngle = pod.heading.value + 18;
            }

            List<Instruction> res = new ArrayList<>();
            for (int angle = minAngle; angle <= maxAngle; angle++) {
                for (int thrust = 0; thrust <= 100; thrust++) {
                    res.add(new Instruction(angle, String.valueOf(thrust)));
                }
                if (mayBoost) {
                    res.add(new Instruction(angle, "BOOST"));
                }
                if (mayShield) {
                    res.add(new Instruction(angle, "SHIELD"));
                }
            }
            return res;
        }

        int getPredictionsAccuracy() {
            int res = 100;
            if (this.total > 0) {
                res = this.accurate * 100 / this.total;
            }
            return res;
        }

        private String getName() {
            return this.name;
        }

        String getInstruction() {
            return this.instruction;
        }

        void setInstruction(String instruction) {
            this.instruction = instruction;
        }

        Pod getPod() {
            return pod;
        }

        void setPod(Pod pod) {
            this.pod = pod;
        }

        private int getLaps() {
            return laps;
        }

        private void setLaps(int laps) {
            this.laps = laps;
        }

        private Instruction fromOutput(String output, Position position) {
            if (output == null) {
                return new Instruction(0, "0");
            }
            String[] tokens = output.split(" ");
            Position direction = new Position(
                    Integer.valueOf(tokens[0]),
                    Integer.valueOf(tokens[1])
            );
            String command = tokens[2];

            int angle = position.angleTo(direction).value;
            return new Instruction(angle, command);
        }

        List<Integer> getPodData() {
            return Arrays.asList(
                    pod.position.x, pod.position.y,
                    pod.speed.x, pod.speed.y,
                    pod.heading.value, pod.target
            );
        }
    }

    static class PilotIdle extends Pilot {

        PilotIdle(Race race) {
            super(race);
        }

        @Override
        PodPrediction getBestChoice(Pod pod) {
            Instruction instruction = new Instruction(0,"0");
            return new PodPrediction(instruction, physics.move(pod, instruction));
        }
    }

    static class PilotRandom extends Pilot {

        PilotRandom(Race race) {
            super(race);
        }

        @Override
        PodPrediction getBestChoice(Pod pod) {
            List<Instruction> options = getAllOptions(pod);
            Instruction instruction = options.get(new Random().nextInt(options.size()));
            return new PodPrediction(instruction, physics.move(pod, instruction));
        }
    }

    static class PilotBrutal extends Pilot {

        PilotBrutal(Race race) {
            super(race);
        }

        @Override
        PodPrediction getBestChoice(Pod pod) {
            PilotBrutalComparator comparator = new PilotBrutalComparator(race.getCheckpointAt(pod.target));
            return getAllOptions(pod, false).stream()
                    .map(instruction -> new PodPrediction(instruction, physics.move(pod, instruction)))
                    .min(comparator)
                    .orElse(null);
        }

    }

    static class PilotAnticipating extends Pilot {

        PilotAnticipating(Race race) {
            super(race);
        }

        @Override
        PodPrediction getBestChoice(Pod pod) {
            List<PodPrediction> predictions = this.getAllOptions(pod, false).stream()
                    .map(instruction -> new PodPrediction(instruction, physics.move(pod, instruction)))
                    .collect(Collectors.toList());

            boolean isCheckPointReachable = false;
            for (PodPrediction current : predictions) {
                if (race.getCheckpointAt(pod.target).distanceTo(current.pod.position) < 600) {
                    isCheckPointReachable = true;
                    break;
                }
            }

            PilotBrutalComparator comparator = new PilotBrutalComparator(race.getCheckpointAt(pod.target));
            if (isCheckPointReachable) {
                comparator = new PilotBrutalComparator(race.getCheckpointAt(pod.target+1));
            }

            return predictions.stream()
                    .min(comparator)
                    .orElse(null);
        }
    }


    static class PilotBrutalComparator implements Comparator<PodPrediction> {

        private Position target;

        PilotBrutalComparator(Position target) {
            this.target = target;
        }

        @Override
        public int compare(PodPrediction first, PodPrediction second) {
            int res;
            if (second == null) {
                return -1;
            } else if (first.equals(second)) {
                return 0;
            } else {
                if (first.pod.position.distanceTo(target) < second.pod.position.distanceTo(target)) {
                    res = -1;
                } else if (first.pod.position.distanceTo(target) > second.pod.position.distanceTo(target)){
                    res = 1;
                } else {
                    int bestAngle = getBestAngle(first, second, target);
                    if (bestAngle < 0) {
                        res = -1;
                    } else {
                        res = 1;
                    }
                }
            }
            return res;
        }

        private int getBestAngle(PodPrediction first, PodPrediction second, Position target) {
            Angle thisAngleToTarget = first.pod.position.angleTo(target);
            Angle thatAngleToTarget = second.pod.position.angleTo(target);

            Angle thisDelta = Angle.of(thisAngleToTarget.value - first.pod.heading.value);
            Angle thatDelta = Angle.of(thatAngleToTarget.value - second.pod.heading.value);

            return Math.abs(thisDelta.value) - Math.abs(thatDelta.value);
        }
    }


    static class PilotWithShield extends Pilot {

        private int shield;

        private PilotBrutal brutal;

        PilotWithShield(Race race) {
            super(race);
            this.brutal = new PilotBrutal(race);
            this.shield = 0;
        }

        @Override
        public PodPrediction getBestChoice(Pod pod) {
            PodPrediction next;
            if(shield == 0 && canGlideToNextCheckpointWithinTicks(3, pod)) {
                PilotBrutalComparator comparator = new PilotBrutalComparator(brutal.race.getCheckpointAt(pod.target));
                PodPrediction prediction = super.getAllOptions(pod).stream()
                        .map(instruction -> new PodPrediction(instruction, brutal.physics.move(pod, instruction)))
                        .min(comparator)
                        .orElse(null);

                if ("BOOST".equals(prediction.instruction.command)) {
                    brutal.mayBoost = false;
                }

                next = prediction;
                next = new PodPrediction(new Instruction(next.instruction.angle.value, "SHIELD"), pod);
            } else if (shield > 0) {
                PilotBrutalComparator comparator = new PilotBrutalComparator(brutal.race.getCheckpointAt(pod.target));
                PodPrediction prediction = super.getAllOptions(pod).stream()
                        .map(instruction -> new PodPrediction(instruction, brutal.physics.move(pod, instruction)))
                        .min(comparator)
                        .orElse(null);

                if ("BOOST".equals(prediction.instruction.command)) {
                    brutal.mayBoost = false;
                }

                next = prediction;
            } else {
                next = brutal.getBestChoice(pod);
            }

            if ("SHIELD".equals(next.instruction.command)) {
                this.shield = 4;
            }
            if (shield > 0) {
                shield--;
            }

            return next;
        }

        boolean canGlideToNextCheckpointWithinTicks(int ticks, Pod pod) {
            boolean res = false;

            for (int i = 0; i < ticks; i++) {
                pod = physics.move(pod, new Instruction(0, "0"));
            }

            if (race.getCheckpointAt(pod.target).distanceTo(pod.position) < 600) {
                res = true;
            }

            return res;
        }
    }

    private static class RaceBuilder {
        private int laps;
        private int count;
        private List<Position> checkpoints = new ArrayList<>();

        private RaceBuilder from(PlayerInput input) {
            this.laps = input.read();
            this.count = input.read();
            for (int i = 0; i < count; i++) {
                checkpoints.add(new Position(input.read(), input.read()));
            }
            return this;
        }

        Race build() {
            return new Race(laps, checkpoints);
        }
    }

    private static class PodBuilder {
        private int x;
        private int y;
        private int vx;
        private int vy;
        private int angle;
        private int nextCheckPointId;

        private PodBuilder from(PlayerInput input) {
            this.x = input.read(); // x position of your pod
            this.y = input.read(); // y position of your pod
            this.vx = input.read(); // x speed of your pod
            this.vy = input.read(); // y speed of your pod
            this.angle = input.read(); // angle of your pod
            this.nextCheckPointId = input.read(); // next check point id of your pod
            return this;
        }

        Pod build() {
            return new Pod(new Position(x, y), new Position(vx, vy), angle, nextCheckPointId);
        }
    }
}